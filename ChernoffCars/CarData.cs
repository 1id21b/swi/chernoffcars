﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChernoffCars
{
    public class CarData
    {
        public List<int> sales;
        public List<int> doors;
        public List<int> persons;
        public List<int> boot;
        public List<int> safety;

        public double midSales;
        public double midDoors;
        public double midPersons;
        public double midBoot;
        public double midSafety;

        public CarData()
        {

            sales = new List<int>();
            doors = new List<int>();
            persons = new List<int>();
            boot = new List<int>();
            safety = new List<int>();
        }

        public CarData(double midSales, double midDoors, double midPersons, double midBoot, double midSafety)
        {
            this.midSales = midSales;
            this.midDoors = midDoors;
            this.midPersons = midPersons;
            this.midBoot = midBoot;
            this.midSafety = midSafety;
        }

        public CarData PreCarData(List<string> Cars)
        {
            int conSales = 0;
            int conDoor = 2;
            int conPersons = 3;
            int conBoot = 4;
            int conSafety = 5;
            List<CarData> carDatas = new List<CarData>();
            foreach (var car in Cars)
            {
                var carParams = car.Split(',');

                switch (carParams[conSales])
                {
                    case "vhigh":
                        sales.Add(30);
                        break;
                    case "high":
                        sales.Add(25);
                        break;
                    case "med":
                        sales.Add(20);
                        break;
                    case "low":
                        sales.Add(10);
                        break;
                    default:
                        sales.Add(5);
                        break;
                }

                switch (carParams[conDoor])
                {
                    case "5":
                        doors.Add(6);
                        break;
                    case "4":
                        doors.Add(4);
                        break;
                    case "3":
                        doors.Add(2);
                        break;
                    case "2":
                        doors.Add(1);
                        break;
                    default:
                        doors.Add(8);
                        break;
                }

                switch (carParams[conPersons])
                {
                    case "more":
                        persons.Add(5);
                        break;
                    case "4":
                        persons.Add(4);
                        break;
                    case "2":
                        persons.Add(2);
                        break;
                    default:
                        persons.Add(5);
                        break;
                }

                switch (carParams[conBoot])
                {
                    case "big":
                        boot.Add(20);
                        break;
                    case "med":
                        boot.Add(10);
                        break;
                    case "small":
                        boot.Add(0);
                        break;
                    default:
                        boot.Add(0);
                        break;
                }

                switch (carParams[conSafety])
                {
                    case "high":
                        safety.Add(10);
                        break;
                    case "med":
                        safety.Add(5);
                        break;
                    case "low":
                        safety.Add(0);
                        break;
                    default:
                        safety.Add(0);
                        break;
                }

            }
            return new CarData(sales.Average(), doors.Average(), persons.Average(), boot.Average(), safety.Average());
        }
        
        public void clearAllLists()
        {
            sales.Clear();
            doors.Clear();
            persons.Clear();
            boot.Clear();
            safety.Clear();
        }
    }
}
