﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChernoffCars
{
    public class CarDrawer
    {
        Pen pen;
        Pen handlePen;
        SolidBrush brush;
        SolidBrush bumperBrush;
        SolidBrush bootBrush;
        SolidBrush darkBrush;

        public CarDrawer()
        {
            darkBrush = new SolidBrush(Color.Black);
            bootBrush = new SolidBrush(Color.Green);
            bumperBrush = new SolidBrush(Color.DarkSlateGray);
            brush = new SolidBrush(Color.Red);
            pen = new Pen(Color.Black, 2);
            handlePen = new Pen(Color.Blue, 1);
        }

        public Graphics DrawACar(int x, int y, int width, int height, int bootOffset, int bumperOffset, int peopleCount, int handleOffset, Values category, Graphics g)
        {


            int wheelSize = height - height / 3;

            Point start = new Point(x, y);
            Point frontStart = new Point(start.X + width, start.Y);
            Size size = new Size(width, height);
            Size bootSize = new Size(size.Width + bootOffset, height + bootOffset);
            Size bumperSize = new Size(width / 10 + bumperOffset, width / 10 + bumperOffset);
            Point bootStart = new Point(start.X - bootOffset, start.Y - bootOffset);
            Point secondOffset = new Point(x + width / 2, y - height);
            Point rearBumper = new Point((bootStart.X - bumperSize.Width), (y + height / 3));
            Point frontBumper = new Point((frontStart.X + size.Width), (y + height / 3));
            Rectangle boot = new Rectangle(bootStart, bootSize);
            Rectangle front = new Rectangle(frontStart, size);
            Rectangle roof = new Rectangle(secondOffset, size);
            Rectangle rearWheel = new Rectangle(x + width / 3, y + wheelSize, wheelSize, wheelSize);
            Rectangle frontWheel = new Rectangle(frontStart.X + width / 3, y + wheelSize, wheelSize, wheelSize);
            Rectangle rearBumperR = new Rectangle(rearBumper, bumperSize);
            Rectangle frontBumperR = new Rectangle(frontBumper, bumperSize);

            Rectangle frontPerson1 = new Rectangle(secondOffset.X + width * 2 / 3, secondOffset.Y + height / 2, wheelSize - height / 4, wheelSize - height / 4);
            Rectangle frontPerson2 = new Rectangle((secondOffset.X + width * 2 / 3) + width / 10, secondOffset.Y + height / 2, wheelSize - height / 4, wheelSize - height / 4);

            Rectangle rearPerson1 = new Rectangle((secondOffset.X + width / 4) + width / 10, secondOffset.Y + height / 2, wheelSize - height / 4, wheelSize - height / 4);
            Rectangle rearPerson2 = new Rectangle(secondOffset.X + width / 4, secondOffset.Y + height / 2, wheelSize - height / 4, wheelSize - height / 4);
            Rectangle rearPerson3 = new Rectangle((secondOffset.X + width / 4) - width / 10, secondOffset.Y + height / 2, wheelSize - height / 4, wheelSize - height / 4);

            Point handleVerA = new Point(frontStart.X + width / 3, frontStart.Y + height / 3);
            Point handleVerB = new Point(frontStart.X + width / 3, frontStart.Y + height / 2 + handleOffset);

            Point handleHorA = new Point((frontStart.X + width / 6) - handleOffset, frontStart.Y + height / 4 + handleOffset);
            Point handleHorB = new Point(frontStart.X + width / 3 + handleOffset, frontStart.Y + height / 4 + handleOffset);


            g.FillRectangle(bootBrush, boot);
            g.FillRectangle(brush, front);
            g.FillRectangle(brush, roof);
            g.FillRectangle(bumperBrush, rearBumperR);
            //g.DrawRectangle(pen, rearBumperR);
            g.DrawEllipse(pen, frontWheel);
            g.DrawEllipse(pen, rearWheel);
            g.FillRectangle(bumperBrush, frontBumperR);
            //g.DrawRectangle(pen, frontBumperR);

            switch (peopleCount)
            {
                case 1:
                    g.DrawEllipse(pen, frontPerson1);
                    break;
                case 2:
                    g.DrawEllipse(pen, frontPerson1);
                    g.DrawEllipse(pen, frontPerson2);
                    g.FillEllipse(brush, frontPerson1);
                    break;
                case 3:
                    g.DrawEllipse(pen, frontPerson1);
                    g.DrawEllipse(pen, frontPerson2);
                    g.FillEllipse(brush, frontPerson1);
                    g.DrawEllipse(pen, rearPerson1);
                    break;
                case 4:
                    g.DrawEllipse(pen, frontPerson1);
                    g.DrawEllipse(pen, frontPerson2);
                    g.FillEllipse(brush, frontPerson1);
                    g.DrawEllipse(pen, rearPerson1);
                    g.DrawEllipse(pen, rearPerson2);
                    g.FillEllipse(brush, rearPerson2);
                    break;
                case 5:
                    g.DrawEllipse(pen, frontPerson1);
                    g.DrawEllipse(pen, frontPerson2);
                    g.FillEllipse(brush, frontPerson1);
                    g.DrawEllipse(pen, rearPerson1);
                    g.DrawEllipse(pen, rearPerson2);
                    g.FillEllipse(brush, rearPerson2);
                    g.FillEllipse(brush, rearPerson3);
                    g.DrawEllipse(pen, rearPerson3);
                    break;
                default:
                    g.DrawEllipse(pen, frontPerson1);
                    break;
            }

            //g.DrawLine(pen, handleVerA, handleVerB);
            handlePen.Width = handleOffset;
            g.DrawLine(handlePen, handleHorA, handleHorB);
            Font font = new Font(new System.Drawing.FontFamily("Arial"), 15, FontStyle.Bold);
            g.DrawString(category.ToString(), font, darkBrush, bootStart.X + width / 4, bootStart.Y + height * 5 / 3);

            Console.WriteLine(rearBumperR);
            return g;
        }

    }
}
