﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ChernoffCars
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();

        }

        private void Form1_Draw_Car(object sender, PaintEventArgs e)
        {
            Graphics g = e.Graphics;
            //Pen pen = new Pen(Color.Black, 2);
            //var brush = new SolidBrush(Color.Red);
            //var bumperBrush = new SolidBrush(Color.DarkSlateGray);
            //var bootBrush = new SolidBrush(Color.DarkBlue);
            CarData car = new CarData();

            string fileData = @"C:\Users\albit\Documents\StudiaMaterialy\SwI\car.data";
            var data = File.ReadAllLines(fileData);

            CarDrawer drawer = new CarDrawer();
            List<List<string>> types = new List<List<string>>();
            types.Add(new List<string>());
            types.Add(new List<string>());
            types.Add(new List<string>());
            types.Add(new List<string>());

            foreach (var d in data)
            {
                if (d.Contains("unacc"))
                    types[(int)Values.unacc].Add(d);
                if (d.Contains("acc"))
                    types[(int)Values.acc].Add(d);
                if (d.Contains("good"))
                    types[(int)Values.good].Add(d);
                if (d.Contains("vgood"))
                    types[(int)Values.vgood].Add(d);
            }


            int x = 0;
            int y = 1;
            int width = 50;
            int height = 25;

            drawer.DrawACar(300, 50, width, height, 0, 0, 5, 0, (Values)4, g);

            foreach (var type in types)
            {
                var tmpData = car.PreCarData(type);
                if (x == 0)
                    drawer.DrawACar(20, y * 200, width + (int)tmpData.midSales, height + (int)tmpData.midSales / 2, (int)tmpData.midBoot, (int)tmpData.midSafety, (int)tmpData.midPersons, (int)tmpData.midDoors, (Values)x, g);
                else
                    drawer.DrawACar(x * 200, y * 200, width + (int)tmpData.midSales, height + (int)tmpData.midSales / 2, (int)tmpData.midBoot, (int)tmpData.midSafety, (int)tmpData.midPersons, (int)tmpData.midDoors, (Values)x, g);
                car.clearAllLists();
                x++;
                if (x == 5)
                {
                    y++;
                    x = 1;
                }
            }

            int bootOffset = 5;
            int bumperOffset = 5;

            //from 1 - 5
            int peopleCount = 5;

            int handleOffset = 0;


            //drawer.DrawACar(x, y, width, height, bootOffset, bumperOffset, peopleCount, handleOffset, "Acceptable", g);
            //drawer.DrawACar(x + 150, y, width, height, bootOffset, bumperOffset, peopleCount, handleOffset, "Medium", g);
            //drawer.DrawACar(x + 300, y, width, height, bootOffset, bumperOffset, peopleCount, handleOffset, "Unacceptable", g);



            //int wheelSize = height - height / 3;

            //Point start = new Point(x, y);
            //Point frontStart = new Point(start.X + width, start.Y);
            //Size size = new Size(width, height);
            //Size bootSize = new Size(size.Width + bootOffset, height + bootOffset);
            //Size bumperSize = new Size(width / 10 + bumperOffset, width / 10 + bumperOffset);
            //Point bootStart = new Point(start.X - bootOffset, start.Y - bootOffset);
            //Point secondOffset = new Point(x + width / 2, y - height);
            //Point rearBumper = new Point((bootStart.X - bumperSize.Width), (y + height / 3));
            //Point frontBumper = new Point((frontStart.X + size.Width), (y + height / 3));
            //Rectangle boot = new Rectangle(bootStart, bootSize);
            //Rectangle front = new Rectangle(frontStart, size);
            //Rectangle roof = new Rectangle(secondOffset, size);
            //Rectangle rearWheel = new Rectangle(x + width / 3, y + wheelSize, wheelSize, wheelSize);
            //Rectangle frontWheel = new Rectangle(frontStart.X + width / 3, y + wheelSize, wheelSize, wheelSize);
            //Rectangle rearBumperR = new Rectangle(rearBumper, bumperSize);
            //Rectangle frontBumperR = new Rectangle(frontBumper, bumperSize);

            //Rectangle frontPerson1 = new Rectangle(secondOffset.X + width * 2 / 3, secondOffset.Y + height / 2, wheelSize - height / 4, wheelSize - height / 4);
            //Rectangle frontPerson2 = new Rectangle((secondOffset.X + width * 2 / 3) + width / 10, secondOffset.Y + height / 2, wheelSize - height / 4, wheelSize - height / 4);

            //Rectangle rearPerson1 = new Rectangle((secondOffset.X + width / 4) + width / 10, secondOffset.Y + height / 2, wheelSize - height / 4, wheelSize - height / 4);
            //Rectangle rearPerson2 = new Rectangle(secondOffset.X + width / 4, secondOffset.Y + height / 2, wheelSize - height / 4, wheelSize - height / 4);
            //Rectangle rearPerson3 = new Rectangle((secondOffset.X + width / 4) - width / 10, secondOffset.Y + height / 2, wheelSize - height / 4, wheelSize - height / 4);

            //Point handleVerA = new Point(frontStart.X + width / 3, frontStart.Y + height / 3);
            //Point handleVerB = new Point(frontStart.X + width / 3, frontStart.Y + height / 2 + handleOffset);

            //Point handleHorA = new Point((frontStart.X + width / 6) - handleOffset, frontStart.Y + height / 2 + handleOffset);
            //Point handleHorB = new Point(frontStart.X + width / 3, frontStart.Y + height / 2 + handleOffset);


            //g.FillRectangle(bootBrush, boot);
            //g.FillRectangle(brush, front);
            //g.FillRectangle(brush, roof);
            //g.FillRectangle(bumperBrush, rearBumperR);
            ////g.DrawRectangle(pen, rearBumperR);
            //g.DrawEllipse(pen, frontWheel);
            //g.DrawEllipse(pen, rearWheel);
            //g.FillRectangle(bumperBrush, frontBumperR);
            ////g.DrawRectangle(pen, frontBumperR);

            //switch (peopleCount)
            //{
            //    case 1:
            //        g.DrawEllipse(pen, frontPerson1);
            //        break;
            //    case 2:
            //        g.DrawEllipse(pen, frontPerson1);
            //        g.DrawEllipse(pen, frontPerson2);
            //        g.FillEllipse(brush, frontPerson1);
            //        break;
            //    case 3:
            //        g.DrawEllipse(pen, frontPerson1);
            //        g.DrawEllipse(pen, frontPerson2);
            //        g.FillEllipse(brush, frontPerson1);
            //        g.DrawEllipse(pen, rearPerson1);
            //        break;
            //    case 4:
            //        g.DrawEllipse(pen, frontPerson1);
            //        g.DrawEllipse(pen, frontPerson2);
            //        g.FillEllipse(brush, frontPerson1);
            //        g.DrawEllipse(pen, rearPerson1);
            //        g.DrawEllipse(pen, rearPerson2);
            //        g.FillEllipse(brush, rearPerson2);
            //        break;
            //    case 5:
            //        g.DrawEllipse(pen, frontPerson1);
            //        g.DrawEllipse(pen, frontPerson2);
            //        g.FillEllipse(brush, frontPerson1);
            //        g.DrawEllipse(pen, rearPerson1);
            //        g.DrawEllipse(pen, rearPerson2);
            //        g.FillEllipse(brush, rearPerson2);
            //        g.FillEllipse(brush, rearPerson3);
            //        g.DrawEllipse(pen, rearPerson3);
            //        break;
            //    default:
            //        g.DrawEllipse(pen, frontPerson1);
            //        break;
            //}

            //g.DrawLine(pen, handleVerA, handleVerB);
            //g.DrawLine(pen, handleHorA, handleHorB);

            //Console.WriteLine(rearBumperR);
            g.Dispose();
        }

        private class Car
        {
            public Dictionary<string, Point> Body;
            public Dictionary<string, Point> Boot;
            public Dictionary<string, Point> rearBumper;
            public Dictionary<string, Point> frontBumper;
            public Dictionary<string, Point> rearWheel;
            public Dictionary<string, Point> frontWheel;
            public Dictionary<string, Point> rearPassengr;
            public Dictionary<string, Point> frontPassenger;

            public Car(Dictionary<string, Point> body, Dictionary<string, Point> boot, Dictionary<string, Point> rearBumper, Dictionary<string, Point> frontBumper, Dictionary<string, Point> rearWheel, Dictionary<string, Point> frontWheel, Dictionary<string, Point> rearPassengr, Dictionary<string, Point> frontPassenger)
            {
                Body = body;
                Boot = boot;
                this.rearBumper = rearBumper;
                this.frontBumper = frontBumper;
                this.rearWheel = rearWheel;
                this.frontWheel = frontWheel;
                this.rearPassengr = rearPassengr;
                this.frontPassenger = frontPassenger;
            }
        }
    }
}
